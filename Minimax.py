def minimaxValue(gameState, depth, maximizingPlayer):
    # gameState: game state to be evaluated
    # depth: search depth
    # maximizingPlayer: True if maximizing player's turn

    # return: value of game state

    # Recursion has reached the search depth or a terminal node
    if depth == 0 or TicTacToe(gameState).evaluate()!=0:
        return Heuristic(gameState) # Not yet implemented
    
    if maximizingPlayer:
        bestValue = -float('inf')
        children = gameState.getChildren()
        for child in children:
            val = minimaxValue(child, depth-1, False)
            bestVal = max(bestVal, val)
        return bestVal
    else:
        bestVal = float('inf')
        children = gameState.getChildren()
        for child in children:
            val = minimaxValue(child, depth-1, True)
            bestVal = min(bestVal, val)
        return bestVal

def minimaxDecision(gameState, depth):
    # gameState: current game state
    # depth: search depth
    
    # return: gameState with highest value

    bestValue = -float('inf')
    bestMove = None
    
    children = gamestate.getChildren()
    for child in children:
        if minimaxValue(child, depth-1, True) > bestValue:
            bestMove = child

    return bestMove
        
    
