# GameState.py: define the gamestate base class

class GameState:
    def __init__(self):
        self.children = []
    
    # get children returns a list of possible children of this state
    def getChildren(self):
        if not self.children:
            self.children = self.generateChildren()
        return self.children

    def generateChildren(self):
        raise NotImplementedError

    # evaluate the Gamestate based on some heuristic
    def evaluate(self):
        raise NotImplementedError
