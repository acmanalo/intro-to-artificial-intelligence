# ttt.py: modeling of tic tac toe

from GameState import *

# class to represent a tic-tac-toe gamestate
#
# the board is represented as a list of 9 elements, they can either
# be 'X', 'O', or ' ' (empty)
class TicTacToe(GameState):
    def __init__(self, board, nextPlayer):
        super(TicTacToe, self).__init__()
        self.board = board
        self.nextPlayer = nextPlayer

    @staticmethod
    def initialBoard():
        ib = [' '] * 9
        im = 'X'
        return TicTacToe(ib, im)

    def generateChildren(self):
        c = []
        if not self.hasChildren():
            return c
            
        if self.nextPlayer == 'X':
            np_child = 'O'
        else:
            np_child = 'X'

        for i in range(0, len(self.board)):
            if self.board[i] == ' ':
                nb = list(self.board)
                nb[i] = self.nextPlayer
                c.append(TicTacToe(nb,np_child))
        return c

    def evaluate(self):
        # winning moves for O receive a score of -inf, winning moves
        # for X receive a score of +inf

        options = [['X', float('inf')],['O', float('-inf')]]
        for xx in range(0,2):
            player,score = options[xx]

            # test rows
            if all(x == player for x in self.board[0:3]):
                return score
            if all(x == player for x in self.board[3:6]):
                return score
            if all(x == player for x in self.board[6:9]):
                return score

            # test columns
            for i in range(0,3):
                c = []
                for j in range(0,3):
                    c.append(self.board[i + (3*j)])
                if all(x == player for x in c):
                    return score

            # test diagonals
            if all(x == player for x in [self.board[i] for i in (0,4,8)]):
                return score
            if all(x == player for x in [self.board[i] for i in (2,4,6)]):
                return score

        # else return 0
        return 0

    def hasChildren(self):
        val = self.evaluate()
        return not ((val == float('inf')) or (val == float('-inf')))
            
        
    def draw(self):
        print("\n",self.board[0],"|",self.board[1],"|",self.board[2],sep='')
        print("-----",sep='')
        print(self.board[3],"|",self.board[4],"|",self.board[5],sep='')
        print("-----",sep='')
        print(self.board[6],"|",self.board[7],"|",self.board[8], "\n",sep='')
                
    
